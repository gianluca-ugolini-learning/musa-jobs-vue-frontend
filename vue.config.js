module.exports = {
    devServer: {
        proxy: 'http://localhost:3000',
        port: 3000
    },
    chainWebpack: config => {
        config
            .plugin('html')
            .tap((args) => {
                args[0].title = 'Musa Job Opportunities';
                return args;
            });
    },
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import "@/assets/scss/app.scss"; @import "@/assets/scss/_variables.scss";`
            }
        }
    }
};