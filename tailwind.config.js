module.exports = {
  purge: [],
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
    extend: {
      colors: {
        "mj": {
          'green-1': '#7bca55',
          'blue-1': '#1a68b3'
        }
      }
    }
  },
  variants: {
    backgroundOpacity: ['responsive', 'hover', 'focus', 'disabled'],
  },
  plugins: [],
}
