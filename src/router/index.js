import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    beforeEnter: ( to, from, next ) => {
      if( store.getters.isAuth ) {
        next( '/list' );
      } else {
        next();
      }
    }
  },
  {
    path: '/register-company',
    name: 'RegisterCompany',
    component: () => import('../views/RegisterCompany.vue'),
    beforeEnter: ( to, from, next ) => {
      if( store.getters.isAuth ) {
        next( '/list' );
      } else {
        next();
      }
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    children: [
      {
        path: '/',
        name: 'UserLogin',
        component: () => import('../views/UserLogin.vue'),
      },
      {
        path: 'company',
        name: 'CompanyLogin',
        component: () => import('../views/CompanyLogin.vue'),
      }
    ],
    beforeEnter: ( to, from, next ) => {
      if( store.getters.isAuth ) {
        next( '/list' );
      } else {
        next();
      }
    }
  },
  {
    path: '/inputs',
    name: 'Inputs',
    component: () => import('../views/Inputs.vue')
  },
  {
    path: '/list',
    name: 'List',
    component: () => import('../views/List.vue'),
    beforeEnter: ( to, from, next ) => {
      if( store.getters.isAuth ) {
        next();
      } else {
        next('/login');
      }
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/Profile.vue'),
    beforeEnter: ( to, from, next ) => {
      if( store.getters.isAuth ) {
        next();
      } else {
        next('/login');
      }
    }
  },
  {
    path: '/not-found',
    name: 'NotFound',
    component: () => import('../views/404.vue')
  },
  { path: '*', redirect: '/not-found' }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
