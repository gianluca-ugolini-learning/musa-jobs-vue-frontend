import axios from 'axios'
import router from '../../router'

const state = {
    token: localStorage.getItem('token') || null,
    userData: {},
    errors: {}
};
const getters = {
    token: state => {
        return state.token;
    },
    user: state => {
        return state.userData;
    },
    isAuth: state => {
        return state.token !== null;
    },
    errors: state => {
        return state.errors;
    },
    userType: state => {
        switch( state.userData.role_id ) {
            case 2:
                return 'user';
            case 3:
                return 'company';
            default: 
                return 'none';
        }
    },
    isUser: state => {
        return state.userData.role_id === 2;
    },
    isCompany: state => {
        return state.userData.role_id === 3;
    }
};
const mutations = {
    login: ( state, payload ) => {
        state.token = payload.token;
        state.userData = payload.userData;
        localStorage.setItem( 'token', payload.token );
        
        router.push('/list');
    },
    logout: ( state ) => {
        state.token = null;
        state.userData = {};
        localStorage.removeItem( 'token' );

        router.push('/');
    },
    setUserData: ( state, payload ) => {
        state.userData = payload;
    },
    setErrors: (state, payload) => {
        state.errors = payload;
    },
    resetErrors: (state) => {
        state.errors = {};
    },
    setUserAvatar: (state, payload) => {
        state.userData.details.photo = payload;
        state.userData.details.photo_full_path = payload;
    }
};
const actions = {
    doLogin: ({commit}, payload) => {
        axios
            .post('user-login', payload )
            .then( res => {
                commit( 'login', res.data.result );
            } )
            .catch( err => {
                console.log( 'error', err );
            });
    },
    doCompanyLogin: ({commit}, payload) => {
        axios
            .post('company-login', payload )
            .then( res => {
                commit( 'login', res.data.result );
            } )
            .catch( err => {
                console.log( 'error', err );
            });
    },
    doLogout: ({commit}) => {

        axios
            .post( 'logout' )
            .then( res => {
                if( res.status === 200 ) {
                    commit( 'logout' );
                } else {
                    throw new Error('errore di logout');
                }
            })
            .catch( err => {
                console.log( err );
            });
    },
    checkLogin: ({commit}) => {
        if( state.token ) {
            axios
                .post( 'my-profile' )
                .then( res => {
                    if( res.status === 200 ) {
                        commit( 'setUserData', res.data.result.userData );
                    }
                })
                .catch( () => {
                    commit( 'logout' );
                });
        }
    },
    registerCompany: ({commit}, payload) => {
        const postBody = {
            first_name: payload.firstName,
            last_name: payload.lastName,
            email: payload.email,
            company_name: payload.companyName,
            category: payload.category,
            password: payload.password,
            confirm_password: payload.passwordConfirm,
            privacy: payload.privacy
        };

        commit( 'resetErrors' );
        axios
            .post( 'register-company', postBody )
            .then( res => {
                commit( 'login', res.data.result );
            })
            .catch( err => {
                commit( 'setErrors', err.response.data.message );
                // console.log( err );
                // console.log( err.response );
                // console.log( err.request );
                // console.log( err.message );
                // console.log( err.config );
            });
    },
    registerUser: ({commit}, payload) => {
        const postBody = {
            first_name: payload.firstName,
            last_name: payload.lastName,
            email: payload.email,
            password: payload.password,
            confirm_password: payload.passwordConfirm,
            privacy: payload.privacy
        };

        commit( 'resetErrors' );
        axios
            .post( 'register', postBody )
            .then( res => {
                commit( 'login', res.data.result );
            })
            .catch( err => {
                commit( 'setErrors', err.response.data.message );
                // console.log( err );
                // console.log( err.response );
                // console.log( err.request );
                // console.log( err.message );
                // console.log( err.config );
            });
    },
    setErrors: ( {commit}, payload ) => {
        commit( 'setErrors', payload.response.data.message );
    },
    resetErrors: ( {commit} ) => {
        commit( 'resetErrors' );
    },
    setUserAvatar( { commit }, payload ) {
        commit( 'setUserAvatar', payload );
    }
};


export default {
    state,
    getters,
    mutations,
    actions
}