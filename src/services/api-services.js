import axios from 'axios'

class ApiServices {

    uploadUserPhoto( file, onUploadProgress ) {

        const formData = new FormData();
        formData.append( 'photo', file );
                
        return axios
            .post( 'upload-user-photo', formData, {
            onUploadProgress
        } );
    }

    manageSkill( data ) {
        return axios.post( 'user-skill-add', data );
    }

    removeSkill( id ) {
        return axios.post( 'user-skill-remove', { skill_id: id } );
    }
}

export default new ApiServices();