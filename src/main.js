import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import Vuelidate from 'vuelidate'

import '@/assets/scss/tailwind.scss'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faBriefcase, faAngleLeft, faAngleDown, faAngleUp, faPencilAlt, faTimes, faPlus, faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// axios.defaults.baseURL = 'http://localhost:8000/api/';
axios.defaults.baseURL = 'http://musa-jobs.test/api/';
// axios.defaults.baseURL = 'https://mj-api-pluriahost.it/api/';
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['Accept'] = 'application/json';

axios.interceptors.request.use( config => {
  // console.log( 'req', config );

  const storeToken = store.getters.token;

  if( storeToken ) {
    // config.headers.Authorization = 'Bearer ' + storeToken;
    config.headers.Authorization = `Bearer ${storeToken}`;
  }

  return config;
} );

axios.interceptors.response.use( config => {
  // console.log( 'res', config );
  return config;
} );

Vue.use(Vuelidate)

library.add(faSearch, faUser, faBriefcase, faAngleLeft, faAngleDown, faAngleUp, faPencilAlt, faTimes, faPlus, faThumbsUp);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
